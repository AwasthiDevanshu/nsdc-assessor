export const localDb = {
  methods: {
    save: async function(key, data, type= "local" ) {
      let store = type === "local" ? localStorage : sessionStorage;
      const parsed = JSON.stringify(data);
      store.setItem(key, parsed);
    },
    get: function(key, type= "local" ) {
      let store = type === "local" ? localStorage : sessionStorage;
      if (store.getItem(key)) {
        try {
          return JSON.parse(store.getItem(key));
        } catch (e) {
          store.removeItem(key);
        }
      }
    },
    remove: async function(key, type = "local" ) {
      let store = type === "local" ? localStorage : sessionStorage;
      store.removeItem(key);
    },
    formatTime: function(val) {
      if (val < 10) {
        return "0" + val;
      }
      return val;
    },
    getToken: function() {
      if (localStorage.getItem("assessorToken")) {
        try {
          var token = this.get("assessorToken");
          var tokenExpiry = this.get("assessorTokenExpiry");
          var expiryTime = tokenExpiry;
          var currentTimeStamp = Math.round(new Date().getTime() / 1000);
          if (expiryTime > currentTimeStamp) {
            return token;
          }
        } catch (e) {
          this.remove("assessorToken");
        }
      }
      this.$router.push("/");
    },
  },
};
