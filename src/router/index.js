import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// auth
const Login = () => import('@/auth/Login')
const SignUp = () => import('@/auth/SignUp')

//Conatiner
const Container = () => import('@/components/Container')

const EventList = () => import('@/views/event/EventList')
const CandidateList = () => import('@/views/event/CandidateList')

const router = new Router({
    mode: 'history',
    routes: configRoutes()
});

function configRoutes() {
    return [
        

        {
            path: '/',
            name: 'Assessor',
            component: Container,
            meta: {
                requireAuth: true,
            },
            children: [
                {
                    path: 'eventList',
                    name: 'EventList',
                    component: EventList,
                },
                {
                    path: 'candidates/:eventId',
                    name: 'CandidateList',
                    component: CandidateList
                },
            ]
        },
        {
            path: '/auth/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/auth/register',
            name: 'Sign Up',
            component: SignUp
        },
    ]
}

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requireAuth)) {
        if (to.name === 'Assessor') { 
            localStorage.clear()
            sessionStorage.clear()
            next();
        }
        const auth = localStorage.getItem("assessorToken");
        if (!auth) next({path: '/auth/login'});
        else next();
    } else next();
})


export default router